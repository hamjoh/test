#!/bin/sh

echo "Running release script"
branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
version=$(awk -F ':[ \t]*' '/^.*"version"/ {print $2}' version.json | sed 's/\"//g')
git describe master tag


if [ "$branch" == "master" ]; then
	echo "Tagging and creating release in master"
	echo $version
fi

major=0
minor=0
build=0

# break down the version number into it's components
regex="([0-9]+).([0-9]+).([0-9]+)"
if [[ $version =~ $regex ]]; then
  major="${BASH_REMATCH[1]}"
  minor="${BASH_REMATCH[2]}"
  build="${BASH_REMATCH[3]}"
fi

# check paramater to see which number to increment
if [[ "$1" == "feature" ]]; then
  minor=$(echo $minor + 1 | bc)
elif [[ "$1" == "minor" ]]; then
  build=$(echo $build + 1 | bc)
elif [[ "$1" == "major" ]]; then
  major=$(echo $major+1 | bc)
else
  echo "usage: ./release-script.sh [major/feature/minor]"
  exit -1
fi

# echo the new version number
echo "new version: ${major}.${minor}.${build}"
